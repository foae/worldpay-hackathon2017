const app = require('express')();
const http = require('http').Server(app);
const Io = require('socket.io')(http);

http.listen(3000, '0.0.0.0', () => {
    console.log(`[${(new Date()).toISOString()}][STARTUP] Listening on *:3000`);
});

app.get('/', (Request, Response) => {
    Response.header('Pragma', 'no-cache');
    Response.header('Expires', '-1');
    Response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    Response.sendFile(__dirname + "/app.html");
});

app.get('/authorization', (Request, Response) => {
    Response.header('Pragma', 'no-cache');
    Response.header('Expires', '-1');
    Response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    Response.header('Content-Type', 'application/json');
    console.log(`POS authorized: ${Request.headers.host}`);
    Response.send(
        {
            "firstName": "John",
            "lastName": "thingy",
            "expMonth": 1,
            "expYear": 2018,
            "cardNumber": "5555555555554444",
            "type": "Card",
            "cvc": "123",
            "id": "12345678",
            "token": "12345678"
        }
    );
    
    /*
    hceCard.firstName = config.get('hceCard.firstName');
    hceCard.lastName =  config.get('hceCard.lastName');
    hceCard.expMonth = config.get('hceCard.expMonth');
    hceCard.expYear = config.get('hceCard.expYear');
    hceCard.cardNumber = config.get('hceCard.cardNumber');
    hceCard.type = config.get('hceCard.type');
    hceCard.cvc = config.get('hceCard.cvc');
    */
});


Io.on('connection', (Socket) => {
    
    console.log(`[${(new Date()).toISOString()}][HELLO] User connected.`);
    
    Socket.on('disconnect', () => {
        console.log(`[${(new Date()).toISOString()}][BYE] User disconnected!`);
    });
    
    Socket.on('newChatMessage', (Msg) => {
        console.log(`[${(new Date()).toISOString()}]New message: ${Msg}`);
        Io.emit('newChatMessage', Msg);
    });
});
