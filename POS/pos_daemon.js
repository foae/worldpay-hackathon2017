const app = require('express')();
const http = require('http').Server(app);
const Io = require('socket.io')(http);
let exec = require('child_process').exec, child;
const ab2str = require('arraybuffer-to-string');

const RedisClient = require("redis").createClient({host: '192.168.11.113'});
// const RedisClient = require("redis").createClient();
RedisClient.select(0);

http.listen(3333, '0.0.0.0', () => {
    console.log(`[${(new Date()).toISOString()}][STARTUP POS WEB] Listening on *:3333`);
});

app.get('/', (Request, Response) => {
    // Response.header('Pragma', 'no-cache');
    // Response.header('Expires', '-1');
    // Response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    Response.sendFile(__dirname + "/pos.html");
});

Io.on('connection', (Socket) => {
    
    console.log(`[${(new Date()).toISOString()}][HELLO] User connected.`);
    
    Socket.on('disconnect', () => {
        console.log(`[${(new Date()).toISOString()}][BYE] User disconnected!`);
    });
    
    Socket.on('newChatMessage', (Msg) => {
        console.log(`[${(new Date()).toISOString()}]New message: `);
        console.log(Msg);
        //Io.emit('newChatMessage', Msg);
    });
    
    Socket.on('addProduct', (Msg) => {
        console.log(`[${(new Date()).toISOString()}]New addProduct event: `);
        console.log(Msg);
    });
    
    Socket.on('newProcessPaymentMessage', () => {
        console.log('newProcessPaymentMessage request');
        
        process.chdir('/home/blana/Projects/WorldPayHackthon2017/POS/sdk');
        child = exec('node sdkProducerShopInventory.js', (err, stdOut, stdErr) => {
            if (err) {
                console.log(err);
            }
        
            if (stdOut) {
                console.log(stdOut);
            }
        
            if (stdErr) {
                console.log(stdErr);
            }
        
            console.log("### FINISHED PRODUCER/SHOP INVENTORY ###");
        });
        child = exec('node sdkConsumerPos.js', (err, stdOut, stdErr) => {
            if (err) {
                console.log(err);
            }
            
            if (stdOut) {
                console.log(stdOut);
            }
            
            if (stdErr) {
                console.log(stdErr);
            }
            
            console.log("### FINISHED CONSUMER/POS ###");
            console.log(stdOut);
            Io.emit('newChatMessage', ab2str(stdOut));
        });
        
        // process.chdir('/home/blana/Projects/WorldPayHackthon2017/POS/sdk');
        // //let _cmdProducer = spawn('node', ['./sdk/example-producer-callbacks.js']);
        //
        //
        // let _cmd = spawn('node', ['example-consumer.js']);
        //
        // _cmd.stdout.on('data', (data) => {
        //     console.log(`stdout: ${data}`);
        //     Io.emit('newChatMessage', ab2str(data));
        // });
        //
        // _cmd.stderr.on('data', (data) => {
        //     console.log(`stderr: ${data}`);
        // });
        //
        // _cmd.on('close', (code) => {
        //     console.log(`child process exited with code ${code}`);
        // });
        
    });
});

let brpopCartList = function () {
    RedisClient.blpop('Cart:List', 1, function (error, data) {
        if (error) {
            console.error('[Cart:List] There has been an error:', error);
        }
        
        if (data) {
            console.error('[Cart:List] We have retrieved data from the front of the queue:', data);
            Io.emit('newProductMessage', data);
        }
        
        brpopCartList();
    });
};
brpopCartList();

let brpopTokenList = function () {
    RedisClient.blpop('Token:List', 1, function (error, data) {
        if (error) {
            console.error('[Token:List] There has been an error:', error);
        }
        
        if (data) {
            console.error('[Token:List] We have retrieved data from the front of the queue:', data);
            Io.emit('newClientMessage', data);
        }
        
        brpopTokenList();
    });
};
brpopTokenList();